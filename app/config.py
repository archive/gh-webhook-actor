import os

GITHUB_WEBHOOK_SECRET = os.environ.get("GITHUB_WEBHOOK_SECRET")

SENTRY_DSN = os.environ.get("SENTRY_DSN")
