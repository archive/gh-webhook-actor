#!/usr/bin/env python3
import logging

import sentry_sdk
from flask import Flask, abort, jsonify
from github_webhook import Webhook
from sentry_sdk.integrations.flask import FlaskIntegration

from . import config

logging.basicConfig(level=logging.INFO)

if config.SENTRY_DSN:
    sentry_sdk.init(dsn=config, integrations=[FlaskIntegration()])

app = Flask(__name__)
webhook = Webhook(app, secret=config.GITHUB_WEBHOOK_SECRET)


@webhook.hook(event_type="ping")
def ping(data):
    return abort(jsonify({"ping": "pong"}))
