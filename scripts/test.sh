#!/usr/bin/env python3

set -e

echo "> Running unit-tests"
nose2

echo "> Running black"
black --check app/ tests/

echo "> Running isort"
isort -rc -c app/ tests/

echo "> Running mypy"
mypy --ignore-missing-imports app/ tests/

echo "> Running flake8"
flake8 --ignore=E128,E501 app
flake8 --ignore=E128,E501 tests
