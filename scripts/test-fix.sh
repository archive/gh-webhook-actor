#!/usr/bin/env python3

set -e

black app/ tests/

isort -rc app/ tests/
