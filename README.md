# gh-webhook-actor

[![CircleCI](https://circleci.com/gh/RealOrangeOne/gh-webhook-actor.svg?style=svg)](https://circleci.com/gh/RealOrangeOne/gh-webhook-actor)

Acts on GitHub webhook events, and acts upon them
