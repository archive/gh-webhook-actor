from tests import BaseTestCase


class PingEventTestCase(BaseTestCase):
    def test_pongs(self):
        response = self.send_webhook("ping")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {"ping": "pong"})
