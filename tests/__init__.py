from unittest import TestCase
from app.server import app
import uuid


class BaseTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.app = app.test_client()

    def send_webhook(self, event_type, data={}):
        return self.app.post(
            "/postreceive",
            json=data,
            headers={
                "X-GitHub-Event": event_type,
                "X-GitHub-Delivery": str(uuid.uuid4()),
            },
        )
